# Week in review template

This is bare-bones and hacked together, but it does the job it needs to do.

## Adding an entry
This should be as simple as cloning / forking this repo and running `yarn nextPost`.

You'll be prompted to answer the questions in bash.
It's an awful interface for writing and editing, but that's deliberate.
Be sloppy, and speak your mind.

Once the questions have been answered, you'll be asked if you want to commit the entry now.
If you hit yes (and your repo is all set up) it should commit the entry and kickstart a GitLab pages build.

If you decide you don't want to commit just yet, you can do so later by running `yarn commit`

## Getting it running

This isn't really intended to be run locally, but if you want to tweak the output, you can do.
There's a template in `src/templates` and a dodgy build script in `bin/generateIndex.js`.
You can run the build by entering `yarn build`.
There's no fancy webpack, live-reload, or any of your usual quality of life tools here.
Feel free to add them, but they shouldn't be needed.

#!/bin/bash

today=$(date +'%b %d %Y')
message="Adds the entry for $today"

git add .
git commit -m "$message"
git push
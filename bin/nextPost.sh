#!/bin/bash

dir=src/posts/
today=$(date +'%Y-%m-%d')
content="# $today"
filename=$dir$today.md
day=$(date +'%d')

declare -a questions=(
    "How do you feel about the past week?"
    "What progress did I make on my top priority?"
    "What is the top priority next week?"
    "What was my biggest success or failure this week?"
    "How confident do I feel as a manager?"
)
declare -a answers

if [[ $day -lt 7 ]]
    then questions+=(
        "What are three things you did well last month?"
        "What are three things you want to improve on next month?"
    )
fi

for question in "${questions[@]}"; do
    clear
    echo "${question}"
    read tmp
    answers+=("$tmp")
done

for i in "${!questions[@]}"; do
    content+="\n\n"
    content+="## ${questions[i]}"
    content+="\n"
    content+="${answers[i]}"
done

clear
echo "$content" > $filename
echo "Entry added to $filename"
echo ""
echo "Do you wish to commit this entry now?"
select yn in "Yes" "No"; do
    case $yn in
        Yes ) yarn commit; break;;
        No ) exit;
    esac
done

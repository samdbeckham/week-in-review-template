const fs = require("fs");
const marked = require("marked");

// The path is relative to where the script was called
const POSTS_DIR = "./src/posts";
const TEMPLATE_DIR = "./src/templates";
const OUTPUT_DIR = "./public";

const mainTemplate = fs.readFileSync(`${TEMPLATE_DIR}/index.html`, "utf-8");

const getPosts = new Promise((res, rej) => {
  return fs.readdir(POSTS_DIR, (err, filenames) => {
    if (err) {
      rej(err);
    }
    let posts = "";
    filenames.forEach((filename) => {
      const file = fs.readFileSync(`${POSTS_DIR}/${filename}`, "utf-8");
      posts = `<article class="post">${marked(file)}</article>` + posts;
    });
    res(posts);
  });
});

getPosts.then((posts) => {
  const generatedFile = mainTemplate.replace(
    /<template>.*<\/template>/gm,
    posts
  );

  fs.writeFile(
    `${OUTPUT_DIR}/index.html`,
    generatedFile,
    "utf-8",
    function (err) {
      if (err) {
        console.log("template error", err);
      }
    }
  );
});
